### Summary

When viewing the commit network graph, if there are commits with a long commit message then the box that shows a commit message when rolling over a commit does not properly contain the message. The message seems to overflow *upwards*

### Steps to reproduce

- Make and push a commit with multiple lines in the commit message
- Browse to the commit graph
- Rollover the commit that was just made to see the message

### Expected behavior

Message is contained within the box; the box expands to fit any size content or longer content is correctly truncated.

### Actual behavior

Content pushes up through the top of the box